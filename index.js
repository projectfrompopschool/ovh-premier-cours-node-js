const express = require('express');
const bodyParser = require('body-parser');
const userRouter = require('./route/user_route');
const voitureRouter = require('./route/voiture_route');
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use("/eleve",userRouter);
app.use("/voiture",voitureRouter);


// console.log("hello world!");

app.get('/', (req, res) => {
    res.send('Hello World!')
})



app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})