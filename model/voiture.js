class Voiture {
    
    constructor(nom,marque,km){
        this.id;
        this.nom = nom;
        this.marque = marque;
        this.km = km;
    }

    fromJson(json){
        this.id = json["id"];
        this.nom = json["nom"];
        this.marque = json["marque"];
        this.km = json["km"];
    }
}

module.exports = Voiture;