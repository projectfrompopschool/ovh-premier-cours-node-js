class User {
    
    constructor(name){
        this.id;
        this.name = name;
    }

    fromJson(json){
        this.id = json["id"];
        this.name = json["name"];
    }
}

module.exports = User;