const userModel = require('../model/user');
const db = require('../config/db');

// function getAllEleve(){
//     try {
//         const eleves = [];
//         db.query('SELECT * FROM user', function (error, results, fields) {
//             if (error) throw error;
//             // console.log(results);
//             for(let x = 0; x < results.length; x++){
//                 let eleve = new userModel();
//                 eleve.fromJson(results[x]);
//                 eleves.push(eleve);
//             }
//           });
//           return eleves;
//     } catch (e) {

//     }
// }

async function getAllEleveWithAsync(){
    // console.log(result);
    try {
        const eleves = [];
        const result = await db.awaitQuery('SELECT * FROM user');
        for(let x = 0; x < result.length; x++){
            let eleve = new userModel();
            eleve.fromJson(result[x]);
            eleves.push(eleve);
        }
        return {"status":200,"data":eleves};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getEleveById(id){
    // const eleves = [];
    const result = await db.awaitQuery('SELECT * FROM user WHERE id=?', [id] );
    // let eleve = new userModel();
    // eleve.fromJson(result);
    // eleves.push(eleve);
    // console.log(result);
    return {"status":200,"data":result};
}

async function getDeleteById(id){
    await db.awaitQuery('DELETE FROM user WHERE id=?', [id] );
}

async function updateEleve(eleve){
    await db.awaitQuery('UPDATE user SET name=? WHERE id=?', [eleve.name, eleve.id] );
}

async function addEleve(eleve){
    await db.awaitQuery('INSERT INTO user (name) VALUES (?)', [eleve.name] );
}

module.exports = {
    // getAllEleve,
    updateEleve,
    getDeleteById,
    getEleveById,
    addEleve,
    getAllEleveWithAsync
}