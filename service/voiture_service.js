const userModel = require('../model/voiture');
const db = require('../config/db');

async function getAllVoiture(){
    try {
        const voitures = [];
        const result = await db.awaitQuery('SELECT * FROM voiture');
        for(let x = 0; x < result.length; x++){
            let voiture = new userModel();
            voiture.fromJson(result[x]);
            voitures.push(voiture);
        }
        return {"status":200,"data":voitures};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getVoitureById(id){
    const result = await db.awaitQuery('SELECT * FROM voiture WHERE id=?', [id] );
    return {"status":200,"data":result};
}

async function getDeleteById(id){
    await db.awaitQuery('DELETE FROM voiture WHERE id=?', [id] );
}

async function updateVoiture(voiture){
    await db.awaitQuery('UPDATE voiture SET nom=? WHERE id=?', [voiture.nom, voiture.id] );
}

async function addVoiture(voiture){
    await db.awaitQuery('INSERT INTO voiture (nom) VALUES (?)', [voiture.nom] );
}

module.exports = {
    updateVoiture,
    getDeleteById,
    getVoitureById,
    addVoiture,
    getAllVoiture
}