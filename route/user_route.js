const express = require('express');
const userService = require('../service/user_service');
const userModel = require('../model/user');
const router = express.Router();

router.get('/all', async (req, res) => {
    console.log(await userService.getAllEleveWithAsync())
    res.send('Bienvenue sur l\'espace eleves')
})

router.get('/all/:id', async (req, res) => {
    // console.log(req.params);
    console.log(await userService.getEleveById(req.params["id"]));
    res.send('je suis étudiant N° '+req.params["id"] * 30);
})

router.get('/delete/eleve/:id', (req, res) => {
    userService.getDeleteById(req.params["id"]);
    res.send('je suis étudiant N° '+req.params["id"]);
})

router.post('/add',async (req, res) => {
    let eleve = new userModel();
    // console.log(eleve);
    eleve.fromJson(req.body);
    await userService.addEleve(eleve);
    // console.log(userService.addEleve());
    res.send('votre eleve a était ajouter');
})

router.put('/update/:id', async (req, res) => {
    let eleve = new userModel(req.body);
    // console.log(eleve);
    eleve.fromJson(req.body);
    await userService.updateEleve(eleve);
    res.send('je suis étudiant N° '+req.params["id"]);
})

module.exports = router;