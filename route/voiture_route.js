const express = require('express');
const voitureService = require('../service/voiture_service');
const voitureModel = require('../model/voiture');
const router = express.Router();

router.get('/all', async (req, res) => {
    console.log(await voitureService.getAllVoiture())
    res.send('Bienvenue sur l\'espace voiture')
})

router.get('/voiture/:id', async (req, res) => {
    console.log(await voitureService.getVoitureById(req.params["id"]));
    res.send('voiture N° '+req.params["id"]);
})

router.get('/delete/eleve/:id', (req, res) => {
    userService.getDeleteById(req.params["id"]);
    res.send('je suis étudiant N° '+req.params["id"]);
})

router.post('/add',async (req, res) => {
    let eleve = new userModel();
    // console.log(eleve);
    eleve.fromJson(req.body);
    await userService.addEleve(eleve);
    // console.log(userService.addEleve());
    res.send('votre eleve a était ajouter');
})

router.put('/update/:id', async (req, res) => {
    let eleve = new userModel(req.body);
    // console.log(eleve);
    eleve.fromJson(req.body);
    await userService.updateEleve(eleve);
    res.send('je suis étudiant N° '+req.params["id"]);
})

module.exports = router;